import telebot
import constants
import re
import time
import textforbot
import main
import sqlite3
import random
import menu

bot = telebot.TeleBot(constants.token)

def write(mci, text):
     bot.send_message(mci, text)


def texters (message):
     rez = menu.db_select_pointerForMemoryZak(message)
     if message.text == "а что там с ментами?" :
        result = menu.db_select_pointerForZakon(message)
        if result == 0 and rez == 0:
            menu.first_menu(message)
        elif result == 1:
            user_markup = telebot.types.ReplyKeyboardMarkup(True, True)
            user_markup.row("Давай таки поговорим о Украине")
            user_markup.row("Давай таки поговорим о России")
            user_markup.row('законы Украины о траве (с поясненниями)')
            user_markup.row("законы России о траве (с поясненниями)")
            user_markup.row("истории с ментами")
            user_markup.row("На главное меню")
            bot.send_message(message.chat.id, "Идём далее", reply_markup=user_markup)
     if message.text == "Давай таки поговорим о Украине":
        if rez == 13:
            menu.about_Ukraine(message)
        if rez == 1 :
            menu.ne_davai_dalshe(message)
        if rez == 2:
            menu.da_hochu(message)
        if rez == 4:
            menu.pri_obiske(message)
        if rez == 5:
            menu.otkaz_ot_obiska(message)
        if rez == 6:
            menu.prodolzhai(message)
        if rez == 7:
            menu.zabiraut_v_otd(message)
        if rez == 8:
            menu.cho_s_povestkoi(message)
        if rez == 9:
            menu.delo_v_sude(message)
        if rez == 10:
            menu.sklonenie(message)
        if rez == 11:
            menu.ya_bariga(message)


     if message.text == 'законы Украины о траве (с поясненниями)':
          um = telebot.types.InlineKeyboardMarkup()
          but = telebot.types.InlineKeyboardButton("употребление🤙", callback_data="употребление")
          but1 = telebot.types.InlineKeyboardButton("хранение🎒", callback_data="хранение")
          but2 = telebot.types.InlineKeyboardButton("распространение🛵", callback_data="распространение")
          but3 = telebot.types.InlineKeyboardButton("склонение💁", callback_data="склонение")
          but4 = telebot.types.InlineKeyboardButton("задержание👮", callback_data="задержание")
          but5 = telebot.types.InlineKeyboardButton("безоплатная правовая помощь🤝", callback_data="помощь")
          but6 = telebot.types.InlineKeyboardButton("\U000021A9Выход", callback_data="end")
          um.add(but)
          um.add(but1)
          um.add(but2)
          um.add(but3)
          um.add(but4)
          um.add(but5)
          um.add(but6)
          bot.send_message(message.chat.id, "Выбирай", reply_markup=um)





     if message.text == "\U000021A9 Не, давай позже" or message.text == "Устал. Спс, что спросил."or message.text == 'Может, потом?' or message.text =="Слишком много инфы, бро. Давай передохнем." or message.text == "Ясно, понял. Пока инфы достаточно." or message.text =="Не, с меня пока хватит"  or message.text =="\U0001F3FBСпасибо за инфу, бро, пока хватит" or message.text == "Спасибо, друг, я все понял."  or message.text ==  "Ок, все ясно." or message.text == "Получить справочник по законодательным вопросам" or message.text=="На главное меню" or message.text == "Не, давай покороче, потом побазарим":
          bot.send_message(message.chat.id, "Тогда я продолжу отсюда")
          main.not_first_start(message)
     if message.text == "\U0001F1FA\U0001F1E6 Да, давай о Украине" or message.text == "Давай таки поговорим о Украине":
          menu.before_Ukraine(message)
          menu.db_commit(message,13)
     if message.text == "Да, я отчаянный, начинай\U0001F44C":
          menu.about_Ukraine(message)
          menu.db_commit(message,1)
     if message.text == "Не, давай дальше.":
          menu.ne_davai_dalshe(message)
          menu.db_commit(message,2)
     if message.text == "Да, хочу" :
         menu.da_hochu(message)
         menu.db_commit(message,4)
     if message.text =="\U0001F640 Так что делать при обыске?":
         menu.pri_obiske(message)
         menu.db_commit(message,5)
     if message.text  == "Я могу отказаться от обыска? \U0001F513":
         menu.otkaz_ot_obiska(message)
         menu.db_commit(message,6)
     if message.text == "\U0001F44CПродолжай":
         menu.prodolzhai(message)
         menu.db_commit(message,7)
     if message.text == "А если забирают в отделение? \U0001F694":
         menu.zabiraut_v_otd(message)
         menu.db_commit(message,8)
     if message.text == "Да, что там с повесткой?\U0001F4DD":
         menu.cho_s_povestkoi(message)
         menu.db_commit(message,9)
     if message.text == "Если дело дошло до суда… ⚖":
         menu.delo_v_sude(message)
         menu.db_commit(message,10)
     if message.text == "!!!Меня могут обвинить в склонении к употреблению наркотиков?":
         menu.sklonenie(message)
         menu.db_commit(message,11)
     if message.text == "А если говорят, что я барыга?\U0001F198":
         menu.ya_bariga(message)
         menu.db_commit(message,12)
     if message.text == "Получить справочник по законодательным вопросам" or message.text == "Не, давай покороче, потом побазарим":
         connection = sqlite3.connect('users.db')
         cursor = connection.cursor()
         cursor.execute('UPDATE userlist SET pointerForZakon = ? WHERE userid = ?', (1, message.chat.id,))
         cursor.execute('UPDATE userlist SET pointerForMemoryZak = ? WHERE userid = ?', (0, message.chat.id,))
         connection.commit()
         connection.close()
     if message.text == "услышать песню":
         main.checkUser(message)
         main.audio(message)







