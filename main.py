import telebot
import constants
import random
import texter
import sqlite3
import textforbot

bot = telebot.TeleBot(constants.token)
def write(mci,text):
    bot.send_message(mci,text)
def checkUser(message):
     user_id = message.chat.id
     connection = sqlite3.connect('users.db')
     cursor = connection.cursor()
     cursor.execute('SELECT count(*) FROM userlist WHERE userid = ?', (user_id,))
     data = cursor.fetchone()[0]
     if data == 0:
         cursor.execute("INSERT INTO userlist values(?,?,NULL,?,?,?,?,?)", (user_id,constants.keysForAll,constants.audioList,message.from_user.first_name,message.from_user.last_name,0,0))
         connection.commit()
         connection.close()

def audio(message):
    connection = sqlite3.connect('users.db')
    cursor = connection.cursor()
    cursor.execute('SELECT audiolist FROM userlist WHERE userid =?',(message.chat.id,))
    result = cursor.fetchone()[0]
    new_result = result.split(',')
    try:
     if len(new_result) >= 1:
        randomAll = random.choice(new_result)
        f = open('music/' + randomAll, 'rb')
        bot.send_audio(message.chat.id, f, None)
        new_result.remove(randomAll)
        newnew_result = ",".join(new_result)
        cursor.execute('UPDATE userlist SET audiolist = ? WHERE userid = ?', (newnew_result, message.chat.id,))
        connection.commit()
    except:
        write(message.chat.id, 'Це вже всі пісні які були , ' + str(message.from_user.first_name) + " " + str(message.from_user.last_name) + ") ")

def helper(message):
    connection = sqlite3.connect('users.db')
    cursor = connection.cursor()
    cursor.execute('SELECT sentensions FROM userlist WHERE userid =?',(message.chat.id,))
    result = cursor.fetchone()[0]
    new_result = result.split('.')
    try:
     if len(new_result) >= 1:
        randomAll = random.choice(new_result)
        write(message.chat.id, randomAll)
        new_result.remove(randomAll)
        newnew_result = ".".join(new_result)
        cursor.execute('UPDATE userlist SET sentensions = ? WHERE userid = ?', (newnew_result, message.chat.id,))
        connection.commit()
    except:
        write(message.chat.id, 'я тобі вже все розказав , ' + str(message.from_user.first_name) + " " + str(message.from_user.last_name) + ") ")

#######################################################################################################################
@bot.message_handler(commands=['start'])
def handle_start(message):
    checkUser(message)
    user_markup = telebot.types.ReplyKeyboardMarkup(True, True)
    user_markup.row('услышать сказку','услышать песню')
    user_markup.row('факт о траве','а что там с ментами?')
    user_markup.row ('факт обо мне','влияние травы на заболевания')
    user_markup.row('конопляные рецепты','с чего надо курить?')
    user_markup.row('я вообще по другому вопросу')
    bot.send_message(message.chat.id,"""Привет """+ str(message.from_user.first_name) + " "
                     + str(message.from_user.last_name) +""". Я - знахарь Дуй, казацкий маг-характерник, конопляных дел мастер. 
    Научу заклинаниям от ментов, целебным и вредным свойствам марихуаны, расскажу великие истории растаманов всея Руси.
    Маг не выдает всех своих секретов. Пока выбирай из предложенного:"""   , reply_markup = user_markup)
    #bot.send_message(message.chat.id,'Добрий день, ' + str(message.from_user.first_name) + " " + str(message.from_user.last_name) + ") ", reply_markup = user_markup )

def not_first_start(message):
    checkUser(message)
    user_markup = telebot.types.ReplyKeyboardMarkup(True, True)
    user_markup.row('услышать сказку','услышать песню')
    user_markup.row('факт о траве','а что там с ментами?')
    user_markup.row ('факт обо мне','влияние травы на заболевания')
    user_markup.row('конопляные рецепты','с чего надо курить?')
    user_markup.row('я вообще по другому вопросу')
    bot.send_message(message.chat.id,"""Продолжай"""   , reply_markup = user_markup)
    #bot.send_message(message.chat.id,'Добрий день, ' + str(message.from_user.first_name) + " " + str(message.from_user.last_name) + ") ", reply_markup = user_markup )


@bot.message_handler(commands=['someintresting'])
def handle_someintresting(message):
  checkUser(message)
  helper(message)

@bot.message_handler(commands=['audio'])
def handle_audio(message):
    checkUser(message)
    audio(message)


@bot.callback_query_handler(func=lambda call: call.data == 'ignore')
def ignore(call):
    bot.answer_callback_query(call.id, text="")
@bot.callback_query_handler(func=lambda call: call.data == 'end')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text("Жми тогда меню \U00002B07",message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'main')
def golovna(message):
          um = telebot.types.InlineKeyboardMarkup()
          but = telebot.types.InlineKeyboardButton("употребление🤙", callback_data="употребление")
          but1 = telebot.types.InlineKeyboardButton("хранение🎒", callback_data="хранение")
          but2 = telebot.types.InlineKeyboardButton("распространение🛵", callback_data="распространение")
          but3 = telebot.types.InlineKeyboardButton("склонение💁", callback_data="склонение")
          but4 = telebot.types.InlineKeyboardButton("задержание👮", callback_data="задержание")
          but5 = telebot.types.InlineKeyboardButton("безоплатная правовая помощь🤝", callback_data="помощь")
          but6 = telebot.types.InlineKeyboardButton("\U000021A9Выход", callback_data="end")
          um.add(but)
          um.add(but1)
          um.add(but2)
          um.add(but3)
          um.add(but4)
          um.add(but5)
          um.add(but6)
          bot.edit_message_text("Выбирай", message.from_user.id, message.message.message_id,  reply_markup=um)
############################START УПОТРЕБЛЕНИЕ ########################################################################
@bot.callback_query_handler(func=lambda call: call.data == 'употребление')
def upotreblenie(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but2 = telebot.types.InlineKeyboardButton("🤙Употребление наркотических средств, психотропов и их аналогов", callback_data="употребление1")
    but3 = telebot.types.InlineKeyboardButton("\U0001F3D8Организация помещений для употребления, хранения, производства наркотиков", callback_data="употребление2")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9 Выход", callback_data="main")
    markup.add(but2)
    markup.add(but3)
    markup.add(but27)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text("Выбирай",message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'употребление1')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    #bot.send_message(message.from_user.id,textforbot.sds1 )
    but3 = telebot.types.InlineKeyboardButton("Далее\U000027A1", callback_data="употребление1/1")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9Назад", callback_data="употребление")
    markup.add(but27,but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.sds1,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'употребление1/1')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    #bot.send_message(message.from_user.id,textforbot.sds1 )
    but3 = telebot.types.InlineKeyboardButton("Далее\U000027A1", callback_data="употребление1/2")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9Назад", callback_data="употребление1")
    markup.add(but27,but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.sds2,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'употребление1/2')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but1 = telebot.types.InlineKeyboardButton("\U000021A9Выйти", callback_data="main")
    markup.add(but1)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.sds3,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'употребление2')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("\U000027A1Далее", callback_data="употребление2/1")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9Назад", callback_data="употребление")
    markup.add(but27,but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.upotr2,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'употребление2/1')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but = telebot.types.InlineKeyboardButton("\U000021A9Выйти", callback_data="main")
    markup.add(but)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.upotr21,message.from_user.id, message.message.message_id, reply_markup=markup)
################################END OF УПОТРЕБЛЕНИЯ####################################################################


######################################Start ХРАНЕНИЕ ##################################################################
@bot.callback_query_handler(func=lambda call: call.data == 'хранение')
def upotreblenie(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but1 = telebot.types.InlineKeyboardButton("🤙 Небольшие размеры (до 5 грамм)", callback_data="хранение1")
    but2 = telebot.types.InlineKeyboardButton("\U0001F392Крупные размеры (от 5 грамм до полкило)", callback_data="хранение2")
    but3 = telebot.types.InlineKeyboardButton("\U0001F648 Большие и особо большие размеры (больше полкило)", callback_data="хранение3")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9 Назад", callback_data="main")
    markup.add(but1)
    markup.add(but2)
    markup.add(but3)
    markup.add(but27)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text("Выбирай",message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'хранение1')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    #bot.send_message(message.from_user.id,textforbot.sds1 )
    but3 = telebot.types.InlineKeyboardButton("Далее\U000027A1", callback_data="хранение1/1")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9 Назад", callback_data="хранение")
    markup.add(but27,but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.hranenie11,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'хранение1/1')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but1 = telebot.types.InlineKeyboardButton("\U000021A9Выйти", callback_data="main")
    markup.add(but1)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.hranenie12,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'хранение2')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("Далее\U000027A1", callback_data="хранение2/1")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9 Назад", callback_data="хранение")
    markup.add(but27,but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.hranenie21,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'хранение2/1')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but = telebot.types.InlineKeyboardButton("\U000021A9Выйти", callback_data="main")
    markup.add(but)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.hranenie22,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'хранение3')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("Далее\U000027A1", callback_data="хранение3/1")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9 Назад", callback_data="хранение")
    markup.add(but27,but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.hranenie31,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'хранение3/1')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but = telebot.types.InlineKeyboardButton("\U000021A9Выйти", callback_data="main")
    markup.add(but)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.hranenie32,message.from_user.id, message.message.message_id, reply_markup=markup)
######################################END OF ХРАНЕНИЕ##################################################################
######################################Start Склонение##################################################################
@bot.callback_query_handler(func=lambda call: call.data == 'склонение')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("Далее\U000027A1", callback_data="склонение1/1")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9 Назад", callback_data="main")
    markup.add(but27,but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.sklonenie1,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'склонение1/1')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but1 = telebot.types.InlineKeyboardButton("\U000021A9Выйти", callback_data="main")
    markup.add(but1)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.sklonenie2,message.from_user.id, message.message.message_id, reply_markup=markup)
######################################END OF Склонение#################################################################
#####################################Start распространение#############################################################
@bot.callback_query_handler(func=lambda call: call.data == 'распространение')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("Далее\U000027A1", callback_data="распространение1/1")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9 Назад", callback_data="main")
    markup.add(but27,but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.rasprostranenie1,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'распространение1/1')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("Далее\U000027A1", callback_data="распространение1/2")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9 Назад", callback_data="распространение")
    markup.add(but27,but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.rasprostranenie2,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'распространение1/2')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("Далее\U000027A1", callback_data="распространение1/3")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9 Назад", callback_data="распространение1/1")
    markup.add(but27,but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.rasprostranenie3,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'распространение1/3')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but1 = telebot.types.InlineKeyboardButton("\U000021A9Выйти", callback_data="main")
    markup.add(but1)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.rasprostranenie4,message.from_user.id, message.message.message_id, reply_markup=markup)
#####################################END OF распространение############################################################
#####################################START ЗАДЕРЖАНИЕ##################################################################
@bot.callback_query_handler(func=lambda call: call.data == 'задержание')
def upotreblenie(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but1 = telebot.types.InlineKeyboardButton("👮‍♀️Задержание", callback_data="задержание1")
    but2 = telebot.types.InlineKeyboardButton("👀 Поверхностная проверка", callback_data="задержание2")
    but3 = telebot.types.InlineKeyboardButton("📹 Видеосъемка при задержании", callback_data="задержание3")
    but4 = telebot.types.InlineKeyboardButton("⚠️ Административное задержание", callback_data="задержание4")
    but5 = telebot.types.InlineKeyboardButton("🔍 Обыск", callback_data="задержание5")
    but6 = telebot.types.InlineKeyboardButton("📋Протокол", callback_data="задержание6")
    but7 = telebot.types.InlineKeyboardButton("🚓 Задержание лица в криминально-процессуальном кодексе", callback_data="задержание7")
    but8 = telebot.types.InlineKeyboardButton("🕵️‍♀️ Следственные и розыскные действия", callback_data="задержание8")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9 Назад", callback_data="main")
    markup.add(but1)
    markup.add(but2)
    markup.add(but3)
    markup.add(but4)
    markup.add(but5)
    markup.add(but6)
    markup.add(but7)
    markup.add(but8)
    markup.add(but27)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text("Выбирай",message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'задержание1')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("Далее\U000027A1", callback_data="задержание1/1")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9 Назад", callback_data="задержание")
    markup.add(but27,but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.zaderzh1,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'задержание1/1')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("Далее\U000027A1", callback_data="задержание1/2")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9 Назад", callback_data="задержание1")
    markup.add(but27,but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.zaderzh2,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'задержание1/2')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("\U000021A9Выйти", callback_data="main")
    markup.add(but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.zaderzh3,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == "задержание2")
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    #bot.send_message(message.from_user.id,textforbot.sds1 )
    but3 = telebot.types.InlineKeyboardButton("Далее\U000027A1", callback_data="задержание2/1")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9 Назад", callback_data="задержание")
    markup.add(but27,but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.zaderzh21,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'задержание2/1')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but1 = telebot.types.InlineKeyboardButton("\U000021A9Выйти", callback_data="main")
    markup.add(but1)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.zaderzh22,message.from_user.id, message.message.message_id, reply_markup=markup)

@bot.callback_query_handler(func=lambda call: call.data == 'задержание3')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("Далее\U000027A1", callback_data="задержание3/1")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9 Назад", callback_data="задержание")
    markup.add(but27,but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.zaderzh31,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'задержание3/1')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("Далее\U000027A1", callback_data="задержание3/2")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9 Назад", callback_data="задержание3")
    markup.add(but27,but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.zaderzh32,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'задержание3/2')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("\U000021A9Выйти", callback_data="main")
    markup.add(but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.zaderzh33,message.from_user.id, message.message.message_id, reply_markup=markup)

@bot.callback_query_handler(func=lambda call: call.data == 'задержание4')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("Далее", callback_data="задержание4/1")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9 Назад", callback_data="задержание")
    markup.add(but27,but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.zaderzh41,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'задержание4/1')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("\U000021A9Выйти", callback_data="main")
    markup.add(but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.zaderzh42,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'задержание5')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("\U000021A9Назад", callback_data="задержание")
    markup.add(but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.zaderzh5,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'задержание6')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("\U000021A9Назад", callback_data="задержание")
    markup.add(but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.zaderzh6,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'задержание7')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("Далее\U000027A1", callback_data="зад1")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9Назад", callback_data="задержание")
    markup.add(but27,but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.zaderzh71,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'зад1')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("Далее\U000027A1", callback_data="зад2")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9 Назад", callback_data="задержание7")
    markup.add(but27,but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.zaderzh72,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'зад2')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("Далее\U000027A1", callback_data="зад3")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9Назад", callback_data="зад1")
    markup.add(but27,but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.zaderzh73,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'зад3')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("Далее\U000027A1", callback_data="зад4")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9 Назад", callback_data="зад2")
    markup.add(but27,but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.zaderzh74,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'зад4')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("\U000021A9Выход", callback_data="main")
    markup.add(but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.zaderzh75,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'задержание8')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("Далее\U000027A1", callback_data="зад81")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9 Назад", callback_data="задержание")
    markup.add(but27,but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.zaderzh81,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'зад81')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("Далее\U000027A1", callback_data="зад82")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9 Назад", callback_data="задержание8")
    markup.add(but27,but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.zaderzh82,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'зад82')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("\U000021A9Выход", callback_data="main")
    markup.add(but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.zaderzh83,message.from_user.id, message.message.message_id, reply_markup=markup)
#####################################END OF ЗАДЕРЖНИЕ##################################################################
#####################################
@bot.callback_query_handler(func=lambda call: call.data == 'помощь')
def upotreblenie(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but2 = telebot.types.InlineKeyboardButton("👍 Первичная безоплатная правовая помощь", callback_data="помощь1")
    but3 = telebot.types.InlineKeyboardButton("🤝 При задержании(вторичная правовая помощь)", callback_data="помощь2")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9 Назад", callback_data="main")
    markup.add(but2)
    markup.add(but3)
    markup.add(but27)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text("Выбирай",message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'помощь1')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("Далее\U000027A1", callback_data="пом1")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9 Назад", callback_data="помощь")
    markup.add(but27,but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.pom11,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'пом1')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("Далее\U000027A1", callback_data="пом2")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9 Назад", callback_data="помощь1")
    markup.add(but27,but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.pom12,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'пом2')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("Далее\U000027A1", callback_data="пом3")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9 Назад", callback_data="пом1")
    markup.add(but27,but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.pom13,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'пом3')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("\U000021A9Выход", callback_data="main")
    markup.add(but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.pom14,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'помощь2')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("Далее\U000027A1", callback_data="пом21")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9 Назад", callback_data="помощь")
    markup.add(but27,but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.pom21,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'пом21')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("Далее\U000027A1", callback_data="пом22")
    but27 = telebot.types.InlineKeyboardButton("\U000021A9 Назад", callback_data="помощь2")
    markup.add(but27,but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.pom22,message.from_user.id, message.message.message_id, reply_markup=markup)
@bot.callback_query_handler(func=lambda call: call.data == 'пом22')
def opotreblenie1(message):
    markup = telebot.types.InlineKeyboardMarkup()
    but3 = telebot.types.InlineKeyboardButton("\U000021A9Выход", callback_data="main")
    markup.add(but3)
    bot.answer_callback_query(message.id, text="")
    bot.edit_message_text(textforbot.pom23,message.from_user.id, message.message.message_id, reply_markup=markup)
#####################################
@bot.message_handler(content_types=['text'])
def handle_text(message):
    checkUser(message)
    if message.chat.id > 0:
     texter.texters(message) # ВИКЛИК ФУНКЦІЇ ДЛЯ РОЗМОВИ З БОТОМ(ВОНА В texter.py)


def main():
    bot.polling(none_stop=True)

if __name__ == '__main__':
       main()






