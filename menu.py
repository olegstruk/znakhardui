import telebot
import textforbot
import constants
import time
import sqlite3
import texter


bot = telebot.TeleBot(constants.token)



def first_menu(message):
 user_markup = telebot.types.ReplyKeyboardMarkup(True, True)
 user_markup.row('\U0001F1FA\U0001F1E6 Да, давай о Украине')
 user_markup.row('\U0001F1F7\U0001F1FA Да, давай о России')
 user_markup.row('\U000021A9 Не, давай позже')
 bot.send_message(message.chat.id, textforbot.firstment, reply_markup=user_markup)


def before_Ukraine(message):
    user_markup = telebot.types.ReplyKeyboardMarkup(True, True)
    user_markup.row('Да, я отчаянный, начинай\U0001F44C')
    user_markup.row('Не, давай покороче, потом побазарим')
    bot.send_message(message.chat.id, textforbot.novaUkr, reply_markup=user_markup)

def about_Ukraine(message):
    user_markup = telebot.types.ReplyKeyboardMarkup(True, True)
    user_markup.row('Не, давай дальше.')
    user_markup.row('Устал. Спс, что спросил.')
    bot.send_message(message.chat.id, textforbot.ifUkraine11)
    time.sleep(4)
    bot.send_message(message.chat.id, textforbot.ifUkraine12)
    time.sleep(8)
    bot.send_message(message.chat.id, textforbot.ifUkraine13, reply_markup=user_markup)
def ne_davai_dalshe(message):
    user_markup = telebot.types.ReplyKeyboardMarkup(True, True)
    b1 = telebot.types.KeyboardButton("Да, хочу")
    user_markup.row('А можно текстом предыдущую историю?')
    user_markup.row('Может, потом?')
    user_markup.row(b1)
    bot.send_message(message.chat.id, textforbot.ifUkraineIfneustal1)
    time.sleep(7)
    bot.send_message(message.chat.id, textforbot.ifUkraineIfneustal2)
    time.sleep(12)
    bot.send_message(message.chat.id, textforbot.audiohistory)
    time.sleep(12)
    bot.send_message(message.chat.id, textforbot.ifInteresno,reply_markup=user_markup)
def da_hochu(message):
    user_markup = telebot.types.ReplyKeyboardMarkup(True, True)
    b1 = telebot.types.KeyboardButton("\U0001F640 Так что делать при обыске?")
    b2 = telebot.types.KeyboardButton("Я могу отказаться от обыска? \U0001F513")
    user_markup.row(b1)
    user_markup.row(b2)
    # user_markup.row(itembtnc) # ЦЕ НАЙАХУЄННІШИЙ СПОСІБ ПОХОДУ
    bot.send_message(message.chat.id, textforbot.az1)
    time.sleep(3)
    bot.send_message(message.chat.id, textforbot.az2)
    time.sleep(6)
    bot.send_message(message.chat.id, textforbot.az3, reply_markup=user_markup)


def pri_obiske(message):
    user_markup = telebot.types.ReplyKeyboardMarkup(True, True)
    user_markup.row("Я могу отказаться от обыска? \U0001F513")
    bot.send_message(message.chat.id, textforbot.WhatDoIfObshuk1)
    time.sleep(6)
    bot.send_message(message.chat.id, textforbot.WhatDoIfObshuk2)
    time.sleep(6)
    bot.send_message(message.chat.id, textforbot.WhatDoIfObshuk3)
    time.sleep(6)
    bot.send_message(message.chat.id, textforbot.WhatDoIfObshuk4, reply_markup=user_markup)
def otkaz_ot_obiska(message):
    user_markup = telebot.types.ReplyKeyboardMarkup(True, True)
    user_markup.row("\U0001F44CПродолжай")
    user_markup.row("Слишком много инфы, бро. Давай передохнем.")
    bot.send_message(message.chat.id, textforbot.otkaz1)
    time.sleep(6)
    bot.send_message(message.chat.id, textforbot.otkaz2)
    time.sleep(6)
    bot.send_message(message.chat.id, textforbot.otkaz3, reply_markup=user_markup)
def prodolzhai(message):
    user_markup = telebot.types.ReplyKeyboardMarkup(True, True)
    user_markup.row("А если забирают в отделение? \U0001F694")
    user_markup.row("Ясно, понял. Пока инфы достаточно.")
    bot.send_message(message.chat.id, textforbot.prod1)
    time.sleep(6)
    bot.send_message(message.chat.id, textforbot.prod2)
    time.sleep(6)
    bot.send_message(message.chat.id, textforbot.prod3)
    time.sleep(6)
    bot.send_message(message.chat.id, textforbot.prod4)
    time.sleep(6)
    bot.send_message(message.chat.id, textforbot.prod5, reply_markup=user_markup)
def zabiraut_v_otd(message):
    user_markup = telebot.types.ReplyKeyboardMarkup(True, True)
    user_markup.row("Да, что там с повесткой?\U0001F4DD")
    user_markup.row("Не, с меня пока хватит")
    bot.send_message(message.chat.id, textforbot.otd1)
    time.sleep(6)
    bot.send_message(message.chat.id, textforbot.otd2)
    time.sleep(6)
    bot.send_message(message.chat.id, textforbot.otd3)
    time.sleep(6)
    bot.send_message(message.chat.id, textforbot.otd4)
    time.sleep(6)
    bot.send_message(message.chat.id, textforbot.otd5)
    time.sleep(6)
    bot.send_message(message.chat.id, textforbot.otd6)
    time.sleep(6)
    bot.send_message(message.chat.id, textforbot.otd7)
    time.sleep(6)
    bot.send_message(message.chat.id, textforbot.otd8)
    time.sleep(6)
    bot.send_message(message.chat.id, textforbot.otd9)
    time.sleep(6)
    bot.send_message(message.chat.id, textforbot.otd10)
    time.sleep(6)
    bot.send_message(message.chat.id, textforbot.otd11)
    time.sleep(6)
    bot.send_message(message.chat.id, textforbot.otd12, reply_markup=user_markup)
def cho_s_povestkoi(message):
    user_markup = telebot.types.ReplyKeyboardMarkup(True, True)
    user_markup.row("Если дело дошло до суда… ⚖")
    user_markup.row("\U0001F3FBСпасибо за инфу, бро, пока хватит")
    bot.send_message(message.chat.id, textforbot.povestka1)
    time.sleep(6)
    bot.send_message(message.chat.id, textforbot.povestka2)
    time.sleep(6)
    bot.send_message(message.chat.id, textforbot.povestka3)
    time.sleep(6)
    bot.send_message(message.chat.id, textforbot.povestka4, reply_markup=user_markup)
def delo_v_sude(message):
    user_markup = telebot.types.ReplyKeyboardMarkup(True, True)
    user_markup.row("!!!Меня могут обвинить в склонении к употреблению наркотиков?")
    user_markup.row("Спасибо, друг, я все понял.")
    bot.send_message(message.chat.id, textforbot.sud1)
    time.sleep(6)
    bot.send_message(message.chat.id, textforbot.sud2)
    time.sleep(6)
    bot.send_message(message.chat.id, textforbot.sud3, reply_markup=user_markup)
def sklonenie(message):
    user_markup = telebot.types.ReplyKeyboardMarkup(True, True)
    user_markup.row("А если говорят, что я барыга?\U0001F198")
    user_markup.row("Ок, все ясно.")
    bot.send_message(message.chat.id, textforbot.skl1)
    time.sleep(6)
    bot.send_message(message.chat.id, textforbot.skl2, reply_markup=user_markup)
def ya_bariga(message):
    user_markup = telebot.types.ReplyKeyboardMarkup(True, True)
    user_markup.row("Получить справочник по законодательным вопросам")
    bot.send_message(message.chat.id, textforbot.bar1)
    time.sleep(6)
    bot.send_message(message.chat.id, textforbot.bar2)
    time.sleep(6)
    bot.send_message(message.chat.id, textforbot.bar3, reply_markup=user_markup)


def db_select_pointerForZakon(message):
    connection = sqlite3.connect('users.db')
    cursor = connection.cursor()
    cursor.execute('SELECT pointerForZakon FROM userlist WHERE userid =?', (message.chat.id,))
    result = cursor.fetchone()[0]
    return result
def db_select_pointerForMemoryZak(message):
    connection = sqlite3.connect('users.db')
    cursor = connection.cursor()
    cursor.execute('SELECT pointerForMemoryZak FROM userlist WHERE userid =?', (message.chat.id,))
    rez = cursor.fetchone()[0]
    return rez

def db_commit(message,num):
    connection = sqlite3.connect('users.db')
    cursor = connection.cursor()
    cursor.execute('UPDATE userlist SET pointerForMemoryZak = ? WHERE userid = ?', (num, message.chat.id,))
    connection.commit()












    # if message.text == "давай поговоримо":
    #    write(message.chat.id, 'про що хочеш поговорити?', )
    # if message.text == "бувай, Мамай":
    #    write(message.chat.id, 'бувай',)
    # for key in constants.keysForAskMamai:
    #    if re.match(key, message.text):
    #      write(message.chat.id, 'Слухаю')
    # for key in constants.keysForBong:
    #     if re.search(key, message.text):
    #      write(message.chat.id, 'Ну ти паєхавший, кури через бонг')
    # if message.text =="Не, давай позже":
    #    hide_markup = telebot.types.ReplyKeyboardRemove()
    #   bot.send_message(message.chat.id, "UGU" , reply_markup=hide_markup)